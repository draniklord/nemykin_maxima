﻿using System;

namespace Homework_12
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TransportCard transportCard = new TransportCard();

            transportCard.OnPayment += CardOnOnPayment;
            transportCard.OnRefill += CardOnOnRefill;
            transportCard.OnNoMoney += (string balance) => Console.WriteLine($"Недостаточно средств для оплаты проезда. Ваш баланс: {balance}");

            transportCard.Refill(100);
            transportCard.Payment();
            transportCard.Payment();
            transportCard.Payment();
            transportCard.Payment();
            transportCard.Refill(40);
            transportCard.Payment();
            transportCard.Payment();
        }
        private static void CardOnOnRefill(string balance)
        {
            Console.WriteLine($"Пополнение счета карты: {balance}");
        }

        private static void CardOnOnPayment(string price)
        {
            Console.WriteLine($"Списание средств: {price}");
        }
    }
}