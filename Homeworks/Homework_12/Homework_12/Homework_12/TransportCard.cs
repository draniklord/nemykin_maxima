﻿namespace Homework_12
{
    public delegate void MessangeEventHandler(string balance);

    public class TransportCard
    {
        private const int price = 30;
        private int _balance;

        public event MessangeEventHandler OnRefill;
        public event MessangeEventHandler OnPayment;
        public event MessangeEventHandler OnNoMoney;

        public void Refill(int money)
        {
            _balance += money;
            if (money != 0)
            {
                OnRefill?.Invoke(_balance.ToString());
            }
        }

        public void Payment()
        {
            if (_balance >= 30)
            {
                _balance -= price;
                OnPayment?.Invoke(price.ToString());
            }
            else
            {
                OnNoMoney?.Invoke(_balance.ToString());
            }
        }
    }
}