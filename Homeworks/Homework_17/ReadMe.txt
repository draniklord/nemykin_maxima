Создать проект консольной программы с решениями задач:
	● Дано целое число K (> 0) и целочисленная последовательность A. Найти теоретико-множественную
разность двух фрагментов A: первый содержит все четные числа, а второй — все числа с порядковыми
номерами, большими K. В полученной последовательности (не содержащей одинаковых элементов)
поменять порядок элементов на обратный.
	● Даны целые числа K1 и K2 и целочисленные последовательности A и B. Получить последовательность,
содержащую все числа из A, большие K1, и все числа из B, меньшие K2. Отсортировать полученную
последовательность по возрастанию.
	● Исходная последовательность содержит сведения об абитуриентах. Каждый элемент
последовательности включает следующие поля:

<Фамилия> <Год поступления> <Номер школы>

Для каждой школы вывести общее число абитуриентов за все годы и фамилию первого из абитуриентов
этой школы, содержащихся в исходном наборе данных (вначале указывать номер школы, затем число
абитуриентов, затем фамилию). Сведения о каждой школе выводить на новой строке и упорядочивать
по возрастанию номеров школ.