﻿namespace Homework_17
{
    public class Applicants
    {
        public string LastName { get; set; }
        public int YearOfAdmission { get; set; }
        public int SchoolNumber { get; set; }
    }
}