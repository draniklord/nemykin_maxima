﻿using System;
using System.Collections.Generic;

namespace Homework_17
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Random random = new Random();

            int k1 = random.Next(10, 50);
            int k2 = random.Next(50, 100);

            List<int> a = new List<int>();
            List<int> b = new List<int>();

            List<Applicants> applicants = new List<Applicants>();
            applicants.Add(new Applicants { LastName = "Ivanov", YearOfAdmission = 2008, SchoolNumber = 1 });
            applicants.Add(new Applicants { LastName = "Petrov", YearOfAdmission = 2008, SchoolNumber = 1 });
            applicants.Add(new Applicants { LastName = "Sidorov", YearOfAdmission = 2014, SchoolNumber = 2 });
            applicants.Add(new Applicants { LastName = "Sidorov", YearOfAdmission = 2010, SchoolNumber = 2 });
            applicants.Add(new Applicants { LastName = "Sidorov", YearOfAdmission = 2012, SchoolNumber = 2 });
            applicants.Add(new Applicants { LastName = "Sidorov", YearOfAdmission = 2012, SchoolNumber = 3 });
            applicants.Add(new Applicants { LastName = "Zanoza", YearOfAdmission = 2011, SchoolNumber = 3 });


            Console.WriteLine("=====Задание 1=====");
            Tasks.Task1(a, random, k1);

            Console.WriteLine("\n=====Задание 2=====");
            Tasks.Task2(a, b, random, k1, k2);

            Console.WriteLine("\n=====Задание 3=====");
            Tasks.Task3(applicants);
        }
    }
}