﻿using System;

namespace HW_9
{
    public class Printer
    {
        public static void Print<T>(T item) where T : class
        {
            Console.WriteLine("\n############\n");
            Console.WriteLine(item.GetType());
        }
    }
}