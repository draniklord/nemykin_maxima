﻿using System;

namespace HW_9
{
    public class GenericList<T>
    {
        private T[] myArray;
        private int _index;
        
        public GenericList(int item)
        {
            myArray = new T[item];
        }
        
        public void Add(T value)
        {
            if (_index == myArray.Length)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ошибка!");
                
                return;
            }
            
            myArray[_index] = value;
            _index++;
        }
        
        public void Print()
        {
            foreach (var t in myArray)
            {
                Console.WriteLine(t);
            }
        }
    }
}