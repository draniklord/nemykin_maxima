﻿using System;

namespace HW_9
{
    class Program
    {
        static void Main(string[] arg)
        {
            GenericList<string> text = new GenericList<string>(3);
            {
                text.Add( "Привчик");
                text.Add( "I am programmer");
                text.Add( "++--**//Math");
            
                Printer.Print(text);
                text.Print();
            }

            GenericList<int> integer = new GenericList<int>(3);
            {
                integer.Add( 54);
                integer.Add(78);
                integer.Add( 357);
            
                Printer.Print(integer);
                integer.Print();
            }
            
            GenericListHeir<double> db = new GenericListHeir<double>(3);
            {
                db.Add( 16.8);
                db.Add( 321.565);
                db.Add( 1.2);
            
                Printer.Print(db);
                db.Print();
            }
        }
    }
}