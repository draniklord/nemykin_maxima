﻿namespace Ex_1
{
    public interface INamable
    {
        public string Name { get; set; }
    }
}