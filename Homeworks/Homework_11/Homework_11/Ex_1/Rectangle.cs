﻿namespace Ex_1
{
    public class Rectangle : IFigure, INamable
    {
        /// <summary>
        /// Площадь прямоугольника (ввести стороны)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public Rectangle(double a, double b, string name)
        {
            this.a = a;
            this.b = b;
            this.name = name;
        }

        private double a;
        private double b;

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public double GetArea()
        {
            return (a + b) * 2;
        }
    }
}