﻿using System;

namespace Ex_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Rectangle rectangle1 = new Rectangle(3, 8, "номер 1");

            Console.WriteLine($"Площадь прямоугольника {rectangle1.Name} = {rectangle1.GetArea()}");
        }
    }
}