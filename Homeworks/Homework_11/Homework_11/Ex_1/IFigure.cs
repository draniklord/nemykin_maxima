﻿namespace Ex_1
{
    public interface IFigure
    {
        public double GetArea();
    }
}