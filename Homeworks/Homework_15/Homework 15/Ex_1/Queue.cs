﻿using System;

namespace Ex_1
{
    internal class Queue
    {
        private int i, j;
        private int[] array;

        public void Enqueue(int item)
        {
            ++j;

            Array.Resize(ref array, j);

            array[i] = item;
            i++;

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        (array[i], array[j]) = (array[j], array[i]);
                    }
                }
            }
        }

        public int Dequeue()
        {
            int result = 0;

            if (array.Length - 1 >= 0 && array.Length > array.Length - 1)
            {
                int num = array[array.Length - 1];

                Array.Resize(ref array, array.Length - 1);

                result = num;
            }
            else
            {
                Console.Write("Данные отсутствуют!");
            }

            return result;
        }

        public int Peek()
        {
            int result = 0;

            if (array.Length - 1 >= 0 && array.Length > array.Length - 1)
            {
                result = array[array.Length - 1];
            }
            else
            {
                Console.Write("Данные отсутствуют!");
            }

            return result;
        }
    }
}