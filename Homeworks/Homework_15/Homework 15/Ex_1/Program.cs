﻿using System;
using StackLibrary;

namespace Ex_1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //int count = 5;

            GenStack<int> genStack = new GenStack<int>(5);

            Random rand = new Random();

            for (int i = 0; i < genStack.Capacity; i++)
            {
                genStack.Push(rand.Next(-10, 10));
            }

            Console.WriteLine($"Первый элемент: {genStack.Peek()}\n");

            Console.Write("Элементы стека: ");
            for (int i = 0; i < genStack.Capacity; i++)
            {
                Console.Write("\t" + genStack.Pop());
            }           
        }
    }
}