﻿using System;
using LibraryBubble;

namespace BubbleSort
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов массива: ");

            int n = int.Parse(Console.ReadLine());

            int[] myArray = new int[n];

            Random random = new Random();
            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = random.Next(-100, 100);
            }

            Console.WriteLine("Введенный массив: ");

            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write("\t" + myArray[i]);
            }
            Console.WriteLine("\n");

            myArray.BubbleSort();

            Console.WriteLine("Отсортированный массив: ");
            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write("\t" + myArray[i]);
            }
        }
    }
}