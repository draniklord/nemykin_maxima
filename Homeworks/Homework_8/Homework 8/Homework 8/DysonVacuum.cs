﻿using System;

namespace Homework_8
{
    class DysonVacuum: Vacuum
    {
        public DysonVacuum()
        {

        }
        
        private string _model="Dyson";
        
        public override string Model
        {
            get
            { return _model; } set { }
        }
        
        // private string _room = "Зал";
        //
        // public override string Room
        // {
        //     get { return _room;}
        //     set { }
        // }
        //
        // public override void StartCleaning()
        // {
        //     Console.WriteLine("Модель: " + Model);
        //     Console.WriteLine("Зона очистки: " + Room);
        //     base.StartCleaning();
        // }
    }
}