﻿using System;

namespace Homework_8
{
    class Program
    {
        static void Main(string[] args)
        {
            Vacuum[] myArr = new Vacuum[3];
            myArr[0] = new DysonVacuum();
            myArr[1] = new SamsungVacuum();
            myArr[2] = new XiaomiVacuum();
            
            for (int i = 0; i < myArr.Length; i++)
            {
                myArr[i].StartCleaning();
            }
            
            XiaomiVacuum xiaomiVacuum = new XiaomiVacuum();
            xiaomiVacuum.StartCleaning();
        }
    }
}