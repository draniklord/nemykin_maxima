﻿using System;

namespace Homework_8
{
    class SamsungVacuum : Vacuum
    {
        public SamsungVacuum()
        {
           
        }
        
        private string _model="Samsung";

        public override string Model
        {
            get { return _model; }
            set { }
        }
        
        // private string _room = "Кухня";
        //
        // public override string Room
        // {
        //     get { return _room;}
        //     set { }
        // }
        
        public override void StartCleaning()
        {
            Console.WriteLine("Модель: " + Model);
            //Console.WriteLine("Зона очистки: " + Room);
            base.StartCleaning();
        }
    }
}