﻿using System;

namespace Homework_8
{
    class Vacuum
    {
        private string model;
        //private string room;

        public Vacuum()
        {
            
        }
        
        public virtual string Model 
        {
            get { return model; }
            set { model = value; }
        }

        // public virtual string Room
        // {
        //     get { return room;}
        //     set { room = value; }
        // }

        public virtual void StartCleaning()
        {
            Console.WriteLine("Программа уборки запущена\n");
        }
    } 
}