﻿using System;

namespace Ex_1
{
    public enum Eye
    {
        Brown,
        Green,
        Gray,
        Blue
    }
    
    public class PeopleInfo
    {
        private byte _age;
        private float _height;
        private float _weight;
        public Eye Eye;
            
        public string Name { get; set; }
            
        public byte Age
        {
            get => _age;
            set
            {
                if (value > 0 && value < 120)
                    _age = value;
            }
        }

        public float Height
        {
            get => _height;
            set
            {
                if (value > 0 )
                    _height = value;
            }
        }

        public float Weight 
        {
            get => _weight;
            set
            {
                if (value  > 0)
                    _weight  = value;
            }
        }
            
        public void Print()
        {
            Console.WriteLine("-----------------------------------------------------------------------------");
            Console.WriteLine($"Name: {Name} \nAge: {Age} \nHeight: {Height} \nWeight: {Weight} \nEye: {Eye}");
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            PeopleInfo ruslan = new PeopleInfo
            {
                Name = "Ruslan",
                Age = 20,
                Height = 1.78f, 
                Weight = -65.1f, 
                Eye = Eye.Green
            };
            ruslan.Print();
            
            PeopleInfo timur = new PeopleInfo
            {
                Name = "Timur",
                Age = 28,
                Height = 1.82f, 
                Weight = 74.7f,
                Eye = Eye.Blue
            };
            timur.Print();
        }
    }
}