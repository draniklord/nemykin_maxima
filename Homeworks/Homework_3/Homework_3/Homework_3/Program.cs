﻿using System;

namespace Homework_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите имя: ");
            string name = Console.ReadLine();

            Console.Write("\nВведите возраст: ");
            string str = Console.ReadLine();
            int age = Convert.ToInt32(str);

            Console.Write("\nВведите вес(кг): ");
            double weight = double.Parse(Console.ReadLine());

            Console.Clear();

            Console.WriteLine("Данные человека:\n\nИмя: " + name + "\nВозраст: " + age + "\nВес: " + weight);

            Console.ReadLine();
        }
    }
}