﻿using System;

namespace Ex_1
{
    // Программа считает разницу между суммой четных и нечетных чисел, кроме отрицательных
    class Program
    {
        /// <summary>
        /// Формирование массива
        /// </summary>
        /// <returns></returns>
        static public int[] GetArray()
        {
            Console.Write("Введите количество элементов в массиве: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine($"\nВведите {n} элементов массива");
            int[] myArray = new int[n];

            //формирование массива
            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write($"   Элемент массива №{i + 1}: ");
                myArray[i] = int.Parse(Console.ReadLine());
            }

            return myArray;
        }

        /// <summary>
        /// Сумма четных неотрицательных
        /// </summary>
        /// <param name="myArray"></param>
        /// <returns></returns>
        static public int SumEven(int[] myArray)
        {
            int result1 = 0;
            for (int i = 0; i < myArray.Length; i++)
            {
                if (myArray[i] % 2 == 0 && myArray[i] >= 0)
                {
                    result1 += myArray[i];
                }
            }
            Console.WriteLine($"\nРезультат сложения четных чисел массива: {result1}");

            return result1;
        }

        /// <summary>
        /// Сумма нечетных неотрицательных
        /// </summary>
        /// <param name="myArray"></param>
        /// <returns></returns>
        static public int SumOdd(int[] myArray)
        {
            int result2 = 0;
            for (int i = 0; i < myArray.Length; i++)
            {
                if (myArray[i] % 2 != 0 && myArray[i] >= 0)
                {
                    result2 += myArray[i];
                }
            }
            Console.WriteLine($"\nРезультат сложения нечетных чисел массива: {result2}");

            return result2;
        }

        /// <summary>
        /// Разница между суммой четных и нечетных
        /// </summary>
        /// <param name="result1"></param>
        /// <param name="result2"></param>
        /// <returns></returns>
        static public int Diff(int result1, int result2)
        {
            int result = result1 - result2;
            Console.WriteLine("\nРезультат работы программы: " + result);

            return result;
        }

        static void Main(string[] args)
        {
            int[] myArray = GetArray();

            int result1 = SumEven(myArray);

            int result2 = SumOdd(myArray);

            Diff(result1, result2);

            Console.ReadLine();
        }
    }
}