﻿using System;

namespace Main
{
    public sealed class Triangle : Figure
    {
        public Triangle(string name, double firstSide, double secondSide, double thirdSide) : base(name)
        {
            double perimeter = firstSide + secondSide + thirdSide;
            Area = Math.Sqrt(perimeter * (perimeter - firstSide) * (perimeter - secondSide) * (perimeter - thirdSide));
        }

        public override double Area { get; set; }
        public override void GetArea()
        {
            Console.WriteLine("Площадь = " + Area);
        }
    }
}