﻿using System;

namespace Main
{
    public sealed class Rectangle : Figure
    {
        public Rectangle(string name, double a, double b) : base(name)
        {
            Area = a * b;
        }

        public override double Area { get; set; }
        public override void GetArea()
        {
            Console.WriteLine("Площадь = " + Area);
        }
    }
}