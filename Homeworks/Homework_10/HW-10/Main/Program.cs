﻿using System;

namespace Main
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                Console.Write("Введите в консоль название прямоугольника: ");
                string nameRectangle = Console.ReadLine();

                Console.Write("Введите длину прямоугольника: ");
                double length = double.Parse(Console.ReadLine());

                Console.Write("Введите ширину прямоугольника: ");
                double width = double.Parse(Console.ReadLine());

                Console.Clear();   

                var rectangle = new Rectangle(nameRectangle, length, width);
                rectangle.Print();
                rectangle.GetArea();
            }

            var triangle = new Triangle("Треугольник", 4, 2, 4);
            triangle.Print();
            triangle.GetArea();
        }
    }
}