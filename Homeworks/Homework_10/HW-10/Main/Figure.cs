﻿using System;

namespace Main
{
    public abstract class Figure
    {
        public Figure(string name)
        {
            Name = name;
        }
        public string Name { get; set; }

        public abstract double Area { get; set; }
        public abstract void GetArea();

        public virtual void Print()
        {
            Console.WriteLine("\nФигура: " + Name);
        }
    }
}