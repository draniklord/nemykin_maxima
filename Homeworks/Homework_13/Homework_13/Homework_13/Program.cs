﻿using System;
using ClassLibrary1;
using ClassLibrary2;

namespace Homework_13
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TvRemote.PowerOn();
            TvRemote.PowerOff();

            Console.WriteLine();

            Random random = new Random();
            int[] myArray = new int[10];

            for (int i = 0; i < myArray.Length; i++)
            {
                myArray[i] = random.Next(-60, 60);
                Console.Write(myArray[i] + " ");
            }

            Console.WriteLine();
            myArray.MyReplace();
            Console.WriteLine();

            Console.WriteLine();
            object[] objArray = new object[] { 4, "A", "30", 9.263, -4, "Hello World!", "Телевизор", 34324, 662, "G530d" };
            objArray.MyShuffleGeneric();
        }
    }
}