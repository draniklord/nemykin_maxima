﻿using System;

namespace ClassLibrary2
{
    public static class Replace
    {
        public static void MyReplace(this int[] array)
        {
            Console.WriteLine();

            for (int i = array.Length - 1; i >= 0; i--)
            {
                Console.ForegroundColor = ConsoleColor.Blue;

                if (array[i] < 0)
                {
                    Console.Write($"{array[i] = 0}");
                }
                else
                {
                    Console.Write(array[i] + " ");
                }
            }
        }

        public static void MyShuffleGeneric<T>(this T[] tarray)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            Random rand = new Random();

            for (int i = tarray.Length - 1; i >= 1; i--)
            {
                int j = rand.Next(i + 1);
                (tarray[j], tarray[i]) = (tarray[i], tarray[j]);

                Console.WriteLine(tarray[i]);
            }
        }
    }
}