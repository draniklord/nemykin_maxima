﻿using System;

namespace ClassLibrary1
{
    public partial class TvRemote
    {
        public static bool ButtomPower { get; private set; }
    }

    public partial class TvRemote
    {
        public static void PowerOn()
        {
            ButtomPower = true;
            Console.WriteLine("Включение");
        }

        public static void PowerOff()
        {
            ButtomPower = false;
            Console.WriteLine("Выключение");
        }
    }
}