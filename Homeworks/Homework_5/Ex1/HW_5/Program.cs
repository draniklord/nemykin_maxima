﻿using System;

namespace Ex_1
{
    class Program
    {
        // Программа считает разницу между суммой четных и нечетных чисел, кроме отрицательных
        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов в массиве: ");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine($"\nВведите {n} элементов массива");
            int[] myArray = new int[n];

            //формирование массива
            for (int i = 0; i < myArray.Length; i++)
            {
                Console.Write($"   Элемент массива №{i + 1}: ");
                myArray[i] = int.Parse(Console.ReadLine());
            }

            //сумма четных не отрицательных
            int result1 = 0;
            for (int i = 0; i < myArray.Length; i++)
            {
                if (myArray[i] % 2 == 0 && myArray[i] >= 0)
                {
                    result1 += myArray[i];
                }
            }
            Console.WriteLine($"\nРезультат сложения четных чисел массива: {result1}");

            //сумма нечетных не отрицательных
            int result2 = 0;
            for (int i = 0; i < myArray.Length; i++)
            {
                if (myArray[i] % 2 != 0 && myArray[i] >= 0)
                {
                    result2 += myArray[i];
                }
            }
            Console.WriteLine($"\nРезультат сложения нечетных чисел массива: {result2}");

            //разницу между суммой четных и нечетных
            int result = result1 - result2;

            Console.WriteLine("\nРезультат работы программы: " + result);

            Console.ReadLine();
        }
    }
}