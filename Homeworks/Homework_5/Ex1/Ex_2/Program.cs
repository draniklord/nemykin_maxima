﻿using System;

namespace Ex_2
{
    class Program
    {
        //ВЫВОД МАССИВА В ОБРАТНОМ ПОРЯДКЕ

        static void Main(string[] args)
        {
            Console.Write("Введите количество элементов в массиве: ");
            uint n = uint.Parse(Console.ReadLine());

            Console.WriteLine($"\nВведите {n} элементов массива");
            uint[] reversArray = new uint[n];

            //формирование массива
            for (int i = 0; i < reversArray.Length; i++)
            {
                Console.Write($"   Элемент массива №{i + 1}: ");
                reversArray[i] = uint.Parse(Console.ReadLine());
            }

            Console.WriteLine("\nВывод обратного массива: ");

            //int[] reversArray = { 5, 8, 22, 801 };

            for (int i = reversArray.Length - 1; i >= 0; i--)
            {
                Console.Write(reversArray[i] + "\t");
            }

            Console.ReadLine();
        }
    }
}