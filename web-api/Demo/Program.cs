﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>()
                .UseNpgsql("Host=localhost;Port=5432;Database=test1;Username=postgres;Password=ruslan572");

            var context = new DataContext(optionsBuilder.Options);

            context.Database.Migrate();

            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            context.Users.Add(new User()
            {
                Id = 2,
                FirstName = "Ivan",
                LastName = "Ivanov",
                Role = Role.Admin,
            });

            context.Tickets.Add(new Ticket()
            {
                Id = 2,
                MovieName = "Мстители",
                Genre = "Боевик",
                AgeRating = 16,
                UserId = 2,
            });

            context.SaveChanges();
            context.Dispose();

            Console.ReadKey();
        }
    }
}