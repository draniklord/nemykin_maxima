﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    public class CustomTicketConfiguration : IEntityTypeConfiguration<CustomTicket>
    {
        public void Configure(EntityTypeBuilder<CustomTicket> builder)
        {
            builder.ToTable("CustomTickets");

        }
    }
}