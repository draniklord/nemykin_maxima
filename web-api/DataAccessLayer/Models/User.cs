﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    //[Table("Customers")]
    public class User
    {
        [Required]
        public string Login { get; set; }

        [Required]
        public string Password { get; set; }

        [Key]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Отсутствует фамилия")]
        public string LastName { get; set; }

        public int Age { get; set; }

        public Role Role { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}