﻿namespace DataAccessLayer.Models
{
    public enum Role
    {
        Disable = -1,
        Guest = 0,
        User = 1,
        Admin = 2
    }
}