﻿using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    public class ApiError : ActionResult
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        /// Содержание ошибки
        /// </summary>
        public string Message { get; set; }
    }
}