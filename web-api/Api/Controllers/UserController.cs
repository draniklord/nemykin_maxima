﻿using Api.Models;
using Api.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Api.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            return Ok(_userService.GetAll());
        }

        // Как обработать исключение 
        [HttpGet("{id}")]
        public ActionResult GetId(int id)
        {
            return Ok(_userService.GetId(id));
        }

        [HttpPost]
        public async Task Create(UserDto userDto)
        {
            await _userService.Create(userDto);
        }

        [HttpPut("{id}")]
        public ActionResult UpdateLastName(int id, [FromQuery] string lastName)
        {
            var data = _userService.GetId(id);
            if (data == null)
                return NotFound();

            _userService.Update(id, lastName);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var user = _userService.GetId(id);
            if (user == null)
                return NotFound();

            _userService.Delete(id);

            return Ok();
        }
    }
}