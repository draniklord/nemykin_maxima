﻿using Api.Domain.Accounts.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Serilog;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly ILogger<AccountsController> _logger;
        private readonly IMediator _mediator;

        public AccountsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AllowAnonymous]
        [HttpGet("/login")]
        public async Task<ActionResult<string>> Login(string login, string password)
        {
            _logger.LogInformation($"{nameof(Login)} GET Login request!");
            try
            {
                var jwt = await _mediator.Send(new LoginRequest(login, password));

                return Ok(jwt);
            }
            catch (System.Exception)
            {
                return BadRequest();
            }
        }

        [Authorize(Roles = "User,Admin")]
        [HttpGet("/me")]
        public async Task<ActionResult<string>> Me()
        {
            Log.Logger.Debug($"{nameof(Me)} GET Me request!");
            return Ok(HttpContext.User.Claims?.FirstOrDefault(x => x.Type == ClaimsIdentity.DefaultNameClaimType)?.Value);
        }

        [Authorize(Policy = Policies.RequireAdultPolicy)]
        [HttpGet("/age")]
        [ResponseHeader("Header-age", "0000")]
        public ActionResult GetAge()
        {
            var claims = HttpContext.User.Claims;
            return Ok(HttpContext.User.Claims?.FirstOrDefault(x => x.Type == "age")?.Value);
        }
    }
}