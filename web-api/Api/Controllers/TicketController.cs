﻿using Api.Domain.Tickets;
using Api.Domain.Tickets.Command;
using Api.Domain.Tickets.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class TicketController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TicketController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение всех билетов
        /// </summary>
        /// <param name="getTicketsQuery"></param>
        /// <responce code="200">Билеты найдены</responce>
        /// <responce code="204">Билеты не найдены</responce>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TicketDto>>> GetAll([FromQuery] GetTicketsQuery getTicketsQuery)
        {
            try
            {
                var result = await _mediator.Send(getTicketsQuery);
                return Ok(result);
            }
            catch (Exception)
            {
                return new ApiError()
                {
                    Code = 204,
                    Message = "Нет данных!"
                };
            }
        }

        /// <summary>
        /// Получение Билета по ID
        /// </summary>
        /// <param name="id"></param>
        /// <responce code="200">Билет найден</responce>
        /// <responce code="204">Билет не найден</responce>
        /// <returns></returns>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(TicketDetailsDto), 200)]
        [ProducesResponseType(typeof(ApiError), 204)]
        public async Task<ActionResult> Get(/*[FromQuery]*/ int id)
        {
            TicketDetailsDto result;
            var query = new Details(id);

            try
            {
                result = await _mediator.Send(query);
                return Ok(result);
            }
            catch (Exception)
            {
                //return new ApiError()
                //{
                //    Code = 204,
                //    Message = "Билет с таким ID отсутствует!"
                //};

                var error = new ApiError
                {
                    Message = "Билет с таким ID отсутствует!"
                };
                return BadRequest(error);
            }

            //return Ok(result);
        }

        /// <summary>
        /// Изменение названия фильма
        /// </summary>
        /// <param name="name"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Create(string name, int id)
        {
            if (ModelState.IsValid)
            {
                var createTicketCommand = new CreateTicketCommand(name, id);
                await _mediator.Send(createTicketCommand);
                return Ok();
            }

            return BadRequest(ModelState);
        }
    }
}