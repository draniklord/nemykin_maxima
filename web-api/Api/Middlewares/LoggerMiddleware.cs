﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Api.Middlewares
{
    // Возможно, вам потребуется установить пакет Microsoft.AspNetCore.Http.Abstractions в ваш проект.
    public class LoggerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<LoggerMiddleware> _logger;

        public LoggerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<LoggerMiddleware>() ??
                      throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            _logger
                .LogInformation($"Запрос URL: {Microsoft.AspNetCore.Http.Extensions.UriHelper.GetDisplayUrl(httpContext.Request)}");
            _logger.LogError($"Ошибка!");

            await _next(httpContext);
        }
    }
}