﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace Api.Middlewares
{
    // Возможно, вам потребуется установить пакет Microsoft.AspNetCore.Http.Abstractions в ваш проект.
    public class TokenMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly string _token;

        public TokenMiddleware(RequestDelegate next, string token)
        {
            _next = next;
            _token = token;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Headers["token"] == _token)
            {
                await _next(httpContext);
            }
            else
            {
                httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                await httpContext.Response.WriteAsync("Неверный токен!");
            }
        }
    }
}