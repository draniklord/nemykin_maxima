﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Api.Services
{
    public class JwtGenerator : IJwtGenerator
    {
        private readonly ILogger<JwtGenerator> _logger;
        private readonly AppOptions _appOptions;

        public JwtGenerator(IOptions<AppOptions> appOptions, ILogger<JwtGenerator> logger)
        {
            _appOptions = appOptions.Value;
            _logger = logger;
        }

        public string Create(string login, string role, string age)
        {
            _logger.LogInformation(nameof(Create));

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appOptions.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role),
                    new Claim("age", age),
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            var res = tokenHandler.WriteToken(token);
            return res;
        }
    }
}
