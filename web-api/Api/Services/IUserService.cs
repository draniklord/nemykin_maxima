﻿using Api.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.Services
{
    public interface IUserService
    {
        IEnumerable<UserDto> GetAll();
        UserDto GetId(int id);
        Task Create(UserDto userDto);
        void Update(int id, string lastName);
        void Delete(int id);
    }
}
