﻿using Api.Models;
using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Services
{
    public class UserService : IUserService
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public UserService(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task Create(UserDto userDto)
        {
            var user = _mapper.Map<User>(userDto);

            await _dataContext.AddAsync(user);

            await _dataContext.SaveChangesAsync();
        }

        // Возникли трудности реализации
        public void Delete(int id)
        {
            var data = _dataContext.Users.FirstOrDefault(ticket => ticket.Id == id);

            _dataContext.Users.Remove(data ?? throw new InvalidOperationException());

            _dataContext.SaveChanges();
        }

        public IEnumerable<UserDto> GetAll()
        {
            var user = _dataContext.Users.Select(x => new UserDto
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Role = (int)x.Role,
            });

            return _mapper.Map<IEnumerable<UserDto>>(user);
        }

        public UserDto GetId(int id)
        {
            var user = _dataContext.Users
                .AsNoTracking()
                .First(x => x.Id == id);

            return _mapper.Map<UserDto>(user);
        }

        // Возникли трудности реализации
        public void Update(int id, string lastName)
        {
            throw new NotImplementedException();
        }
    }
}