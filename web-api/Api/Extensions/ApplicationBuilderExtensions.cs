﻿using Api.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace Api.Extensions
{
    /// <summary>
    /// Методы расширения, используемые для добавления ПО промежуточного слоя в конвейер HTTP-запросов.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseExceptionHandler(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<ExceptionMiddleware>();
            return applicationBuilder;
        }

        public static IApplicationBuilder UseTokenAuth(this IApplicationBuilder applicationBuilder, string token)
        {
            applicationBuilder.UseMiddleware<TokenMiddleware>(token);
            return applicationBuilder;
        }

        public static IApplicationBuilder UseLogger(this IApplicationBuilder applicationBuilder)
        {
            applicationBuilder.UseMiddleware<LoggerMiddleware>();
            return applicationBuilder;
        }
    }
}