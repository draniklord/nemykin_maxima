using Api.Domain.Tickets;
using Api.Domain.Tickets.Command;
using Api.Models;
using Api.Models.MapProfiles;
using Api.Services;
using DataAccessLayer;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Serilog;
using System;
using System.IO;
using System.Text;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            //Configuration.GetConnectionString();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var appOptions = Configuration.Get<AppOptions>();

            //var appOptions = new AppOptions();
            //Configuration.Bind(appOptions);

            //var connectionString = Configuration.GetConnectionString("DbConnection");

            services.AddDbContext<DataContext>(builder => builder
                //.UseLazyLoadingProxies()
                .UseNpgsql(appOptions.DbConnection));

            services.Configure<AppOptions>(Configuration);
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IJwtGenerator, JwtGenerator>();
            services.AddScoped<IAuthorizationHandler, AdultAuthRequirementHandler>();
            services.AddMediatR(typeof(CreateTicketCommand));

            services.AddControllers()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<UserValidation>());

            services.AddAutoMapper(typeof(UserProfile), typeof(TicketProfile));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Nemykin_Api", Version = "v1.15" });
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Api.xml"));
                //c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "����������, ������� ����",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    BearerFormat = "JWT"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                        },
                        new string[] { }
                    }
                });
            });

            services
               .AddAuthentication(x =>
               {
                   x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                   x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                   x.RequireAuthenticatedSignIn = false;
               })
               .AddJwtBearer(x =>
               {
                   x.RequireHttpsMetadata = false;
                   x.SaveToken = true;
                   x.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuerSigningKey = true,
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("my-secret-key-key-key-key-key-key-key-key")),
                       ValidateIssuer = false,
                       ValidateAudience = false
                   };
               });

            services.AddAuthorization();
            services.AddAuthorization(options =>
                options.AddPolicy(Policies.RequireAdultPolicy,
                    builder => builder.AddRequirements(new AdultAuthRequirement(18))));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IOptions<AppOptions> options, ILoggerFactory loggerFactory)
        {
            //app.UseExceptionHandler();

            //var log = new LoggerConfiguration()
            //    .MinimumLevel.Debug()
            //    .WriteTo.Console()
            //    .WriteTo.File("log.log")
            //    .CreateLogger();
            //Log.Logger = log;
            //loggerFactory.AddSerilog(log);

            if (options.Value.SwaggerEnabled)
            {
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1")); // /swagger/v1/swagger.json", "Api v1
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseReDoc(c => c.RoutePrefix = "docs");
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            //app.UseTokenAuth("0000");

            //app.UseLogger();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}