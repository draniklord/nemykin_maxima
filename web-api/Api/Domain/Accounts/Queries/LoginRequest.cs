﻿using Api.Services;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Domain.Accounts.Queries
{
    public class LoginRequest : IRequest<string>
    {
        public string Login { get; set; }
        public string Password { get; set; }

        public LoginRequest(string login, string password)
        {
            Login = login;
            Password = password;
        }
    }

    public class LoginHandler : IRequestHandler<LoginRequest, string>
    {
        private readonly DataContext _dataContext;
        private readonly IJwtGenerator _jwtGenerator;

        public LoginHandler(DataContext dataContext, IJwtGenerator jwtGenerator)
        {
            _dataContext = dataContext;
            _jwtGenerator = jwtGenerator;
        }

        public async Task<string> Handle(LoginRequest request, CancellationToken cancellationToken)
        {
            var user = await _dataContext.Users.FirstAsync(user1 => user1.Login == request.Login && user1.Password == request.Password);
            var token = _jwtGenerator.Create(user.Login, user.Role.ToString(), user.Age.ToString());
            return token;
        }
    }
}
