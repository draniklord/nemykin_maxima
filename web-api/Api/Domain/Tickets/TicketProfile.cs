﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Domain.Tickets
{
    public class TicketProfile : Profile
    {
        public TicketProfile()
        {
            CreateMap<TicketDto, Ticket>();
            CreateMap<Ticket, TicketDto>();

            CreateMap<TicketDetailsDto, Ticket>();
            CreateMap<Ticket, TicketDetailsDto>();
        }
    }
}