﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Domain.Tickets
{
    /// <summary>
    /// Билет на фильм
    /// </summary>
    public class TicketDetailsDto
    {
        /// <summary>
        /// ID билета
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessage = "Недопустимое значение ID")]
        public int Id { get; set; }

        /// <summary>
        /// Название фильма
        /// </summary>
        [Required(ErrorMessage = "Название фильма не заполнено")]
        [StringLength(30, ErrorMessage = "Недопустимая длина имени")]
        public string MovieName { get; set; }

        /// <summary>
        /// Жанр фильма
        /// </summary>
        [StringLength(50, MinimumLength = 3)]
        public string Genre { get; set; }

        /// <summary>
        /// Возрастной рейтинг
        /// </summary>
        [Range(0, 23, ErrorMessage = "Недопустимое возрастное ограничение")]
        public byte AgeRating { get; set; }

        /// <summary>
        /// Номер заказа
        /// </summary>
        public uint OrderNumber { get; set; }

        /// <summary>
        /// Дата и время покупки
        /// </summary>
        public DateTime SessionTime { get; set; }

        /// <summary>
        /// Цена билета
        /// </summary>
        public uint TicketPrice { get; set; }

        /// <summary>
        /// Номер зала
        /// </summary>
        public byte HallNumber { get; set; }

        /// <summary>
        /// Ряд
        /// </summary>
        public byte Row { get; set; }

        /// <summary>
        /// Место
        /// </summary>
        public uint Seat { get; set; }

        /// <summary>
        /// ID покупателя
        /// </summary>
        public int UserId { get; set; }

        //public UserDto User { get; set; }
    }
}