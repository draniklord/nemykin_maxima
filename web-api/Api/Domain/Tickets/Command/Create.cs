﻿using DataAccessLayer;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Domain.Tickets.Command
{
    public class CreateTicketCommand : IRequest
    {
        public string Name { get; }
        public int UserId { get; }

        public CreateTicketCommand(string name, int userId)
        {
            Name = name;
            UserId = userId;
        }
    }

    public class CreateTicketCommandHandler : IRequestHandler<CreateTicketCommand>
    {
        private DataContext _dataContext;

        public CreateTicketCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<Unit> Handle(CreateTicketCommand request, CancellationToken cancellationToken)
        {
            await _dataContext.Tickets.AddAsync(new DataAccessLayer.Models.Ticket()
            {
                MovieName = request.Name,
                UserId = request.UserId
            }, cancellationToken);

            await _dataContext.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
