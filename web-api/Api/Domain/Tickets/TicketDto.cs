﻿using Api.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace Api.Domain.Tickets
{
    public class TicketDto
    {
        [Key]
        [Range(0, int.MaxValue, ErrorMessage = "Недопустимое значение ID")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Название фильма не заполнено")]
        [StringLength(30, ErrorMessage = "Недопустимая длина имени")]
        public string MovieName { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Genre { get; set; }

        [Range(0, 23, ErrorMessage = "Недопустимое возрастное ограничение")]
        public byte AgeRating { get; set; }

        public uint OrderNumber { get; set; }

        public DateTime SessionTime { get; set; }

        public uint TicketPrice { get; set; }

        public byte HallNumber { get; set; }

        public byte Row { get; set; }

        public uint Seat { get; set; }

        public int UserId { get; set; }

        public UserDto User { get; set; }
    }
}