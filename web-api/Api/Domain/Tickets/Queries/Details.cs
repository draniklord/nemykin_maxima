﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Api.Domain.Tickets.Queries
{
    public class Details : IRequest<TicketDetailsDto>
    {
        public int Id { get; }

        public Details(int id)
        {
            Id = id;
        }
    }

    public class DetailsHandler : IRequestHandler<Details, TicketDetailsDto>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public DetailsHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<TicketDetailsDto> Handle(Details request, CancellationToken cancellationToken)
        {
            var ticket = await _dataContext.Tickets.Include(ticket1 => ticket1.User)
                .AsNoTracking()
                .FirstOrDefaultAsync(ticket1 => ticket1.Id == request.Id, cancellationToken: cancellationToken);
            return _mapper.Map<TicketDetailsDto>(ticket);
        }
    }
}
