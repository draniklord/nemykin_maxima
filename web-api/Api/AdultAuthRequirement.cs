﻿using System.Linq;
using System.Threading.Tasks;
using DataAccessLayer;
using Microsoft.AspNetCore.Authorization;

namespace Api
{
    public class AdultAuthRequirement : IAuthorizationRequirement
    {
        public int MinimumAge { get; set; }

        public AdultAuthRequirement(int minimumAge)
        {
            MinimumAge = minimumAge;
        }
    }

    public class AdultAuthRequirementHandler : AuthorizationHandler<AdultAuthRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdultAuthRequirement requirement)
        {
            var claim = context.User.Claims.FirstOrDefault(claim => claim.Type == "age");
            if (claim != null)
            {
                int age;
                int.TryParse(claim.Value, out age);

                if (age >= requirement.MinimumAge)
                    context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
