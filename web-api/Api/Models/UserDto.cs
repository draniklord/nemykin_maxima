﻿using FluentValidation;

namespace Api.Models
{
    public class UserDto
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public int Role { get; set; }

        //public List<TicketDto> Tickets { get; set; }
    }

    public class UserValidation : AbstractValidator<UserDto>
    {
        public UserValidation()
        {
            RuleFor(user => user.Id).NotNull().GreaterThanOrEqualTo(0).LessThan(int.MaxValue).WithMessage("ID вышел за пределы нумерации от 0 и до 2147483647");
            RuleFor(user => user.FirstName).NotNull().NotEmpty().MaximumLength(30).WithMessage("Отсутсвие имени или превышено кол-во символово (30)");
            RuleFor(user => user.LastName).NotNull().NotEmpty().MaximumLength(30).WithMessage("Отсутсвие фамилии или превышено кол-во символово (30)");
            RuleFor(user => user.Role).LessThanOrEqualTo(2).GreaterThanOrEqualTo(-1).WithMessage("Отсутствуют роли!");
        }
    }
}