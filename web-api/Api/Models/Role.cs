﻿namespace Api.Models
{
    public enum Role
    {
        Disable,
        Guest,
        User,
        Admin
    }
}