﻿using AutoMapper;
using DataAccessLayer.Models;

namespace Api.Models.MapProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserDto, User>()
                .ForMember(user => user.Role, expression => expression
                    .MapFrom(dto => (Role)dto.Role));
            //.ForMember(user => user.Tickets, expression => expression
            //    .MapFrom(dto => dto.Tickets));

            CreateMap<User, UserDto>()
                .ForMember(dto => dto.Role, expression => expression
                    .MapFrom(user => (int)user.Role));
            //.ForMember(dto => dto.Tickets, expression => expression
            //    .MapFrom(user => user.Tickets));
        }
    }
}